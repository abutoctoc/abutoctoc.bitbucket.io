#+TITLE: DOPAMINE & REWARD CIRCUIT, ADDICTION AND LOVE
#+AUTHOR: Abu TocToc
#+TAGS: noexport

[[https://www.youtube.com/watch?v=f7E0mTJQ2KM][Dopamine used to be associated with pleasure, but Science now knows that it is more correct to associate it to the *expectation* of it]]
[[https://www.youtube.com/watch?v=uSEo2miwnZQ][Your brain doesn't crave sugar. It doesn't crave sex. It doesn't crave victory. It wants dopamine*]]
*It's not really dopamine, like the first video shows. It would be more correct but longer to say "the activation of your reward circuit" (in which dopamine plays a central role)
[[https://www.youtube.com/watch?v=de_b7k9kQp0][Another experiment]] to illustrate how the brain, and especially one of its oldest, most primal and most central subscructures, *the part that's more in control of your actions than your own personnality*, the mesolimbic pathway aka the reward circuit, is too primitive (dumb) to understand the difference between having sex and eating chocolate. It doesn't care about harming you, it only understands "pleasure". If it has to make you suffer to get the stimulation it wants, *it will harm you*.
The cerebral substructures that are "intelligent" enough to understand the differtence between hammering a nail into your foot and going to a casino to gamble are incapable of making decisions and are subordinated to the reward circuit; just like the computer that mines crypto is too dumb to decide what to do and instead does what it is told.
Destroying the dopamine-rich neurons in the nucleus accumbens eliminates any desire to use drugs for addicted individuals.

[[https://en.wikipedia.org/wiki/Sex_and_Culture][Sex and Culture (1934), Unwin]]
